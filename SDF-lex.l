%{
/*
    SDF Library for reading Self-Describing Files
    Copyright (C) 1991,1992  John K. Salmon

    Distributed under the terms of the BSD 3-clause License.

    The full license is in the file COPYING.txt, distributed with this software.
*/
#include <stdlib.h>
/* This define means we don't have to link with the lex library! */
#define YY_SKIP_YYWRAP
#define yywrap() 1

/* This one is ugly.  
   Lex's output contains fprintf(yyout,  of various error messages.  
   We try here to redirect them to a Msg_doalist. */
#include <stdarg.h>

#ifdef FLEX_SCANNER
/* Flex DOES have a much better way to handle non-standard input strategies.
   This doesn't have to be this twisted to work with Flex, but if we are
   going to continue to support lex, it's easiest to just follow along */
#define YY_INPUT(buf, result, maxsize) \
  { \
  int c = SDF_Hdrgetc(); \
  /* Msgf(("YY_INPUT:  c=%c\n", c)); */\
  result = (c==EOF) ? YY_NULL : (buf[0] = c, 1); \
  }
/* Flex also has some new-and-different behavior AFTER YY_INPUT has returned
   NULL.  In particular, all subsequent calls to yylex will immediately
   return NULL unless yyrestart is called.  If we were always using Flex,
   I could attach this to the BEGIN rule, but I can't do that with lex,
   so I have to call it from outside, e.g., in SDFyyprepare. */
void SDFlexprepare(void){
    yyrestart(NULL);
}
#else /* Not FLEX_SCANNER */
void SDFlexprepare(void){}
#endif
%}
%option never-interactive
White [ \f\t\n]
Dig [0-9]
Alpha [A-Za-z_]
Hex [A-Fa-f0-9]
Octal [0-7]
Alphanum [A-Za-z0-9_]
Expon [Ee]{Sign}{Dig}+
Punct "="|"["|"]"|"{"|"}"|";"|","
Sign [-+]?
%%
#.*SDF-EOH.* {SDFlineno++; return EOHDR;} /* Terminate the header on a comment line */
#.*	{SDFlineno++;} /* Otherwise, skip comments from '#' to eol. */
{White} {if(yytext[0] == '\n') SDFlineno++;} /* skip white space. count lines*/
"/*"	{ int c;
	  loop:
	  while((c=input()) != '*') {if(c=='\n') SDFlineno++;}
	  switch(input()){
		case '/': break;
		case '*': unput('*');
		default: goto loop;
	  }
	} /* Skip normal C comments. (no nesting) */
char	{yylval.type = SDF_CHAR; return TYPE;}
short	{yylval.type = SDF_INT16; return TYPE;}
int	{yylval.type = SDF_INT32; return TYPE;}
long	{yylval.type = SDF_INT32; return TYPE;}
int8_t {yylval.type = SDF_CHAR; return TYPE;}
int16_t {yylval.type = SDF_INT16; return TYPE;}
int32_t {yylval.type = SDF_INT32; return TYPE;}
int64_t	{yylval.type = SDF_INT64; return TYPE;}
uint8_t {yylval.type = SDF_CHAR; return TYPE;}
uint16_t {yylval.type = SDF_INT16; return TYPE;}
uint32_t {yylval.type = SDF_INT32; return TYPE;}
uint64_t {yylval.type = SDF_INT64; return TYPE;}
float	{yylval.type = SDF_FLOAT; return TYPE;}
double	{yylval.type = SDF_DOUBLE; return TYPE;}
struct	{return STRUCT;}
unsigned ; /* WARNING.  The keyword 'unsigned' is skipped entirely. */
parameter	{return PARAMETER;}
byteorder {yylval.valueparam = BYTEORDER; return VALUEPARAM;}
{Alpha}{Alphanum}* {yylval.string = Malloc(yyleng+1);
	strcpy(yylval.string, (char *)yytext);
#if YYDEBUG
	if(yydebug)
	    printf("lex-malloc(%llu)) at 0x%p\n", 
		   (unsigned long long)yyleng+1, yylval.string);
#endif
	return NAME;}
\"[^"]*\"	{
	/* strings extend to the next double-quote. */
	/* there are no escapes, and no exceptions. */
	/* We fiddle with the yyleng so we only get the contents in yylval. */
	/* Newlines embedded in strings will be missed by SDFlineno! */
	    yylval.constant.u.stringval = Malloc(yyleng-1);
	    strncpy(yylval.constant.u.stringval, (char *)yytext+1, yyleng-2);
	    yylval.constant.u.stringval[yyleng-2] = '\0';
	    yylval.constant.type = SDF_STRING;
#if YYDEBUG
	    if(yydebug)
	        printf("lex-malloc(%llu) = 0x%p\n", (unsigned long long)yyleng-1, 
		    yylval.constant.u.stringval);
#endif
	    return CONST;}
{Sign}{Dig}+"."{Dig}*({Expon})? |
{Sign}{Dig}*"."{Dig}+({Expon})? |
{Sign}{Dig}+{Expon} {
	yylval.constant.type = SDF_DOUBLE;
	sscanf((char *)yytext, "%lf", &yylval.constant.u.doubleval); 
	return CONST;}
0x{Hex}+  {
	yylval.constant.type = SDF_INT64;
        sscanf((char *)yytext+2, "%"PRIx64, &yylval.constant.u.int64val);
	return CONST;}
0{Octal}+ {
	yylval.constant.type = SDF_INT64;
	sscanf((char *)yytext+1, "%"PRIo64, &yylval.constant.u.int64val);
	return CONST;}
{Sign}{Dig}+	{
	yylval.constant.type = SDF_INT64;
	sscanf((char *)yytext, "%"PRId64, &yylval.constant.u.int64val);
	return CONST;}
{Punct} {return yytext[0];}
. {yyerror("lexer confusion on char: <%c>, line: %d\n", (yytext[0])?yytext[0]:'?',
	SDFlineno); return LEXERROR;}
