/*
  SDF Library for reading Self-Describing Files
  Copyright (C) 1991,1992  John K. Salmon

  Distributed under the terms of the BSD 3-clause License.

  The full license is in the file COPYING.txt, distributed with this software.
*/
#ifndef sdfDOTh
#define sdfDOTh
#include <stdarg.h>
#include <stdint.h>
#include <sys/types.h>
#define SDF_SEEK_SET 0
#define SDF_SEEK_CUR 1
#define SDF_SEEK_END 2

#define SDF_SYNC 0
#define SDF_ASYNC 1

#define SDF_SINGL 0
#define SDF_MULTI 1

#ifndef INT64_MAX
#if __WORDSIZE==64
#define INT64_MAX LONG_MAX
#else
/* #define INT64_MAX LLONG_MAX Why doesn't this work? */
#define INT64_MAX 9223372036854775807LL
#endif
#endif

#ifndef sdfprivateDOTh
/* SDF isn't really this, but I don't want to tell you what it is. */
/* If you pretend it's this, then your compiler can check prototypes */
/* for you. */
typedef char *SDF[32];

/* Identical to declaration in SDF-private.h */
enum SDF_type_enum{SDF_NOTYPE, 
		       SDF_CHAR, 
		       SDF_INT16, 
		       SDF_INT32,
		       SDF_INT64,
		       SDF_FLOAT, 
		       SDF_DOUBLE, 
		       SDF_STRING};
/* INT and LONG are identical to INT32 */
#define SDF_INT  SDF_INT32
#define SDF_LONG SDF_INT32
#endif

#ifdef __cplusplus
extern "C" {
#endif
extern char libSDF_version[];
/* Two arrays indexed by a type_enum */
extern char *SDFtype_names[];
extern int SDFtype_sizes[];

extern char SDFerrstring[];

int SDFissdf(const char *filename);		/* Not guaranteed correct! */
SDF *SDFopen(const char *hdrfname, const char *datafname);
int SDFseekable(SDF *hdr);	/* are non-sequential reads allowed? */
int SDFclose(SDF *hdr);
int SDFnvecs(SDF *hdr);
int SDFhasname(const char *name, SDF *hdr);
char **SDFvecnames(SDF *hdr);
int64_t SDFnrecs(const char *name, SDF *hdr);
int SDFarrcnt(const char *name, SDF *hdr);
enum SDF_type_enum SDFtype(const char *name, SDF *hdr);
int SDFseek(const char *name, int64_t offset, int whence, SDF *hdr);
int SDFtell(const char *name, SDF *hdr);
unsigned int SDFcpubyteorder(void);
unsigned int SDFbyteorder(SDF *hdr);
int SDFswap(SDF *hdr);
int SDFnoswap(SDF *hdr);
int SDFisswapping(SDF *hdr);
int SDFsetmaxbufsz(int newsz);
int SDFrdvecs(SDF *hdr, ...
	     /* char *name, int n, void *address, int stride,  
		... , 
		NULL */ );
int SDFrdvecsv(SDF *hdr, va_list ap);
/* Where is the const supposed to go? */
int SDFrdvecsarr(SDF *hdr, int nreq, 
	  char **names, int *ns, void **addresses, int *strides);

int SDFseekrdvecs(SDF *hdr, ...
	     /* char *name, int start, int n, void *addr, int stride,  
		... ,
		NULL */ );
int SDFseekrdvecsv(SDF *hdr, va_list ap);
int SDFseekrdvecsarr(SDF *hdr, int nreq, 
	  char **names, int64_t *starts, int *ns, void **addresses, int *strides);
void SDFsetiomode(int mode);
int SDFdebug(int level);

/* These two subvert the SDF "abstraction" and tell you about */
/* the actual layout of the file. Are you absolutely sure you need */
/* to call these? */
int64_t SDFfileoffset(const char *name, SDF *hdr);
int64_t SDFfilestride(const char *name, SDF *hdr);

/* These are harder to write than one might guess. */
/* They're in the library to avoid duplicating code. */
int SDFgetint16(SDF *sdfp, const char *name, int16_t *value);
int SDFgetint32(SDF *sdfp, const char *name, int32_t *value);
int SDFgetint64(SDF *sdfp, const char *name, int64_t *value);
int SDFgetfloat(SDF *sdfp, const char *name, float *value);
int SDFgetdouble(SDF *sdfp, const char *name, double *value);
int SDFgetstring(SDF *sdfp, const char *name, char *string, int size);
/* SDFgetint and SDFgetlong read 32-bit quantities in
   the file into int or long in the program. */
int SDFgetint(SDF *sdfp, const char *name, int *value);
int SDFgetlong(SDF *sdfp, const char *name, long *value);

#ifdef __cplusplus
}
#endif

/* a few macros that call SDFget and bail out if the value isn't there */
#define SDFgetint16OrDie(sdfp, name, value) \
    do{ if( SDFgetint16(sdfp, name, value) ) \
	    Error("SDFgetint(\"%s\") failed\n", name); } while(0)

#define SDFgetint32OrDie(sdfp, name, value) \
    do{ if( SDFgetint32(sdfp, name, value) ) \
	    Error("SDFgetint(\"%s\") failed\n", name); } while(0)

#define SDFgetint64OrDie(sdfp, name, value) \
    do{ if( SDFgetint64(sdfp, name, value) ) \
	    Error("SDFgetint64(\"%s\") failed\n", name); } while(0)

#define SDFgetfloatOrDie(sdfp, name, value) \
    do{ if( SDFgetfloat(sdfp, name, value) ) \
	    Error("SDFgetfloat(\"%s\") failed\n", name); } while(0)

#define SDFgetdoubleOrDie(sdfp, name, value) \
    do{ if( SDFgetdouble(sdfp, name, value) ) \
	    Error("SDFgetdouble(\"%s\") failed\n", name); } while(0)

#define SDFgetstringOrDie(sdfp, name, string, size) \
    do{ if( SDFgetstring(sdfp, name, string, size) ) \
	    Error("SDFgetstring(\"%s\") failed", name); } while(0)

#define SDFgetintOrDie(sdfp, name, value) \
    do{ if( SDFgetint(sdfp, name, value) ) \
	    Error("SDFgetint(\"%s\") failed\n", name); } while(0)

#define SDFgetlongOrDie(sdfp, name, value) \
    do{ if( SDFgetint32(sdfp, name, value) ) \
	    Error("SDFgetint(\"%s\") failed\n", name); } while(0)

/* And a few more that use a default if the value isn't there */
#define SDFgetint16OrDefault(sdfp, name, value, def) \
    do{ if( SDFgetint16(sdfp, name, value) ){ \
	    *value = def;}} while(0)

#define SDFgetint32OrDefault(sdfp, name, value, def) \
    do{ if( SDFgetint32(sdfp, name, value) ){ \
	    *value = def;}} while(0)

#define SDFgetint64OrDefault(sdfp, name, value, def) \
    do{ if( SDFgetint64(sdfp, name, value) ){ \
	    *value = def;}} while(0)

#define SDFgetfloatOrDefault(sdfp, name, value, def) \
    do{ if( SDFgetfloat(sdfp, name, value) ){ \
	    *value = def;} } while(0)

#define SDFgetdoubleOrDefault(sdfp, name, value, def) \
    do{ if( SDFgetdouble(sdfp, name, value) ){ \
	    *value = def;} } while(0)

#define SDFgetstringOrDefault(sdfp, name, value, size, def) \
    do{ if( SDFgetstring(sdfp, name, value, size) ){ \
	    strncpy(value, def, size);} } while(0)

#define SDFgetintOrDefault(sdfp, name, value, def) \
    do{ if( SDFgetint(sdfp, name, value) ){ \
	    *value = def;}} while(0)

#define SDFgetlongOrDefault(sdfp, name, value, def) \
    do{ if( SDFgetlong(sdfp, name, value) ){ \
	    *value = def;}} while(0)

#endif /* sdfDOTh */
