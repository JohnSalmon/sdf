/* 
   Simple text to SDF converter
   Copyright (C) 2013-2014 Michael S. Warren <mswarren@gmail.com>
   
   Distributed under the terms of the BSD 3-clause License.

   The full license is in the file COPYING.txt, distributed with this software.
*/

#define _GNU_SOURCE		/* for asprinf */
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>

#define Error(...) { fprintf(stderr, __VA_ARGS__); exit(1); }

#define KV_MAX 256
#define DESC_MAX 64
#define STR_MAX 256

typedef struct keyval {
    char key[64];
    double val;
} keyval;

typedef struct options_s {
    int64_t n;
    int cols;
    int hdralign;
    int verbose;
    int blocksize;
    int ndesc;
    int nvar;
    int nkv;
    char sep[STR_MAX];
    char format[STR_MAX];
    char nname[STR_MAX];
    char in[FILENAME_MAX];
    char out[FILENAME_MAX];
    keyval kv[KV_MAX];
    char var[DESC_MAX][STR_MAX];
    char desc[DESC_MAX][STR_MAX];
} options_s;

/* Macro to accumulate strings in a buffer */
#define outstr(buf, str, ...)					\
    {								\
	char *tmp = buf;					\
	char *tmp2 = NULL;					\
	asprintf(&tmp2, str, ##__VA_ARGS__);			\
	asprintf(&(buf), "%s%s", tmp ? tmp : "", tmp2);		\
	free(tmp2);						\
	free(tmp);						\
    }

/* macros with stringification */
#define scan(s, v, f) sscanf(s, #v "=" #f, &opt->v)
#define scans(s, v, f) sscanf(s, #v "=" #f, opt->v)

static void
parse_opt(int argc, char *argv[], options_s *opt)
{
    /* A non-standard arg parsing idiom */
    for (int i = 1; i < argc; i++) {
	char *p = argv[i];
	int ret;
	ret = 0;
	ret += sscanf(p, "n=%"PRId64, &opt->n);
	ret += scan(p, cols, %d);
	ret += scan(p, hdralign, %d);
	ret += scan(p, verbose, %d);
	ret += scan(p, blocksize, %d);
	ret += scans(p, sep, %256c);
	ret += scans(p, format, %256c);
	ret += scans(p, nname, %256s);
	ret += scans(p, in, %256s);
	ret += scans(p, out, %256s);
	if (strncmp(p, "desc=", 5) == 0) {
	    strncpy(opt->desc[opt->ndesc], p+5, STR_MAX);
	    if (opt->ndesc++ > DESC_MAX) Error("Too many desc\n");
	    ret += 1;
	}
	if (strncmp(p, "var=", 4) == 0) {
	    strncpy(opt->var[opt->nvar], p+4, STR_MAX);
	    if (opt->nvar++ > DESC_MAX) Error("Too many vars\n");
	    ret += 1;
	}
	if (ret != 1) {
	    char *c = strtok(p, "=");
	    strncpy(opt->kv[opt->nkv].key, c, STR_MAX);
	    if (sscanf(p+strlen(c)+1, "%lf", &opt->kv[opt->nkv].val) != 1) {
		fprintf(stderr, "Warning: failed to parse %s=%s\n", c, p);
	    } else if (opt->nkv++ >= KV_MAX) {
		Error("nkv too large\n");
	    }
	}
    }
    if (opt->format[0] && (strlen(opt->format) != opt->cols)) 
	Error("Format has %lld elements != to cols of %d\n", (long long)strlen(opt->format), opt->cols);
}

int
main(int argc, char *argv[])
{
    options_s opt = {.sep = ",", .in = "-", .out = "particles.sdf", 
		     .cols = 6, .nname = "npart", .hdralign = 32,
		     .verbose = 1, .blocksize = 1024*1024};

    parse_opt(argc, argv, &opt);

    char *hdr = NULL;
    outstr(hdr, "# SDF 1.0\n");
    unsigned char bytes[4] = {0x12, 0x34, 0x56, 0x78};
    unsigned int *byteorder = (unsigned int *)&bytes[0];
    outstr(hdr, "parameter byteorder = 0x%x;\n", *byteorder);

    /* var is copied verbatim to header */
    for (int i = 0; i < opt.nvar; i++) {
	outstr(hdr, "%s\n", opt.var[i]);
    }

    if (opt.n) outstr(hdr, "int64_t %s = %"PRId64";\n", opt.nname, opt.n);
    /* double key/values */
    for (int i = 0; i < opt.nkv; i++) {
	outstr(hdr, "double %s = %.16g;\n", opt.kv[i].key, opt.kv[i].val);
    }
    outstr(hdr, "struct {\n");
    /* structure descriptors (corresponds to text file column labels) */
    for (int i = 0; i < opt.ndesc; i++) {
	outstr(hdr, "    %s;\n", opt.desc[i]);
    }
    if (opt.n) {
	outstr(hdr, "}[%"PRId64"];\n", opt.n);
    } else {
	outstr(hdr, "}[];\n");
    }
    outstr(hdr, "\f\n# SDF-EOH");
    /* Align data (not required, but nice for od, mmap, etc.) */
    for (int i = strlen(hdr)+1; i % opt.hdralign; i++) { /* +1 to account for \n */
	outstr(hdr, " ");
    }
    outstr(hdr, "\n");

    FILE *infp, *outfp;
    if (!(outfp = fopen(opt.out, "w"))) Error("fopen %s failed\n", opt.out);
    if (fwrite(hdr, 1, strlen(hdr), outfp) != strlen(hdr)) Error("fwrite failed\n");
    if (opt.in[0] == '-') {
	infp = stdin;
    } else if (!(infp = fopen(opt.in, "r"))) Error("fopen %s failed\n", opt.in);

    int ssize = 0;
    if (!opt.format[0]) {
	ssize = opt.cols * sizeof(float);
    } else {
	for (int i = 0; i < opt.cols; i++) {
	    if (opt.format[i] == 'f') ssize += sizeof(float);
	    else if (opt.format[i] == 'd') ssize += sizeof(double);
	    else if (opt.format[i] == 'c') ssize += sizeof(int8_t);
	    else if (opt.format[i] == 'h') ssize += sizeof(int16_t);
	    else if (opt.format[i] == 'i') ssize += sizeof(int32_t);
	    else if (opt.format[i] == 'q') ssize += sizeof(int64_t);
	    else Error("Unimplemented format char\n");
	}
    }
    int64_t mlen = opt.blocksize;
    char *mtab = malloc(mlen * ssize);
    if (!mtab) Error("malloc failed\n");
    char *mptr = mtab;
    int64_t linenum = 0;
    char line[1024];
    while (fgets(line, sizeof(line), infp)) {
	linenum++;
	if (opt.verbose && ((linenum+1) % 1000000 == 0)) {
	    fprintf(stderr, "%"PRId64"M ", (linenum+1)/1000000);
	    fflush(stderr);
	}
	if (line[0] == '#' || line[0] == '\n') continue;
	char tmp[sizeof(line)];
	strncpy(tmp, line, sizeof(line)); /* strtok writes into string */
	char *c = strtok(tmp, opt.sep);
	for (int i = 0; i < opt.cols; i++) {
	    if (!c) Error("parse failed line %"PRId64", %s\n", linenum, line);
	    /* Assume float if no format string */
	    if (!opt.format[0] || opt.format[i] == 'f') {
		*(float *)mptr = strtof(c, NULL);
		mptr += sizeof(float);
	    } else if (opt.format[i] == 'd') {
		*(double *)mptr = strtod(c, NULL);
		mptr += sizeof(double);
	    } else if (opt.format[i] == 'c') {
		*(int8_t *)mptr = strtol(c, NULL, 0);
		mptr += sizeof(int8_t);
	    } else if (opt.format[i] == 'h') {
		*(int16_t *)mptr = strtol(c, NULL, 0);
		mptr += sizeof(int16_t);
	    } else if (opt.format[i] == 'i') {
		*(int32_t *)mptr = strtol(c, NULL, 0);
		mptr += sizeof(int32_t);
	    } else if (opt.format[i] == 'q') {
		*(int64_t *)mptr = strtoll(c, NULL, 0);
		mptr += sizeof(int64_t);
	    }
	    c = strtok(NULL, opt.sep);
	}
	if (mptr-mtab >= mlen*ssize) {
	    if (fwrite(mtab, 1, mptr-mtab, outfp) != mptr-mtab) Error("fwrite failed\n");
	    mptr = mtab;
	}
    }
    if (fwrite(mtab, 1, mptr-mtab, outfp) != mptr-mtab) Error("fwrite failed\n");
    fclose(outfp);
    free(mtab);
    free(hdr);
    if (opt.verbose) fprintf(stderr, "\nDone\n");
    exit(0);
}

/* Example for converting MDR1

./txttosdf \
in=mdr1_particles_z0.1 out=mdr1_1.000 nname=npart n=8589934592 a=1.0 \
Omega0=0.26 Omega0_m=0.26 Omega0_r=0.0 Omega0_lambda=0.74 Omega0_fld=0.0 \
Omega0_b=0.0469 n_s=0.95 sigma_8=0.82 \
h_100=0.70 H0=70.0 H=70.0 R0=500.0 redshift=0.0 \
epsilon_scaled=0.007 part_mass=8.721e9 \
BOX_SIZE=1000.0 SCALE_NOW=1.0 var="int rockstar_units = 1;" \
var='char sql_query[] = "select x,y,z,vx,vy,vz from MDR1..particles where particleID between 850000000000 and 858589934592";' \
var='char sql_query_date[] = "2013-05-13 16:59:59.692 CEST";' \
desc="float x, y, z" desc="float vx, vy, vz"

*/


/* Example for converting Rockstar .list
in=- out=ds13_f_4000_subhalo_0.667 a=0.666667 \
Omega0=0.318340 Omega0_m=0.318340 Omega0_r=0.0 Omega0_lambda=0.681660 Omega0_fld=0.0 \
h_100=0.670423 H0=67.0423 H=70.0 R0=2000.0 redshift=0.5 \
epsilon_scaled=0.0462767 part_mass=8.22651e+10 \
BOX_SIZE=4000.0 SCALE_NOW=0.666667 var="int rockstar_units = 1;" \
desc="int64_t ID, DescID" \
desc="float M200b Vmax Vrms R200b Rs" \
desc="int64_t Np" \
desc="float x, y, z, vx, vy, vz" \
desc="float Jx, Jy, Jz" \
desc="float Spin, rs_klypin" \
desc="float Mvir, M200c, M500c, M2500c" \
desc="float Xoff, Vfof, spin_bullock, b_to_a, c_to_a" \
desc="float Ax, Ay, Az" \
desc="float kin_to_pot" \
var="#ID DescID M200b Vmax Vrms R200b Rs Np X Y Z VX VY VZ JX JY JZ Spin rs_klypin Mvir M200c M500c M2500c Xoff Voff spin_bullock b_to_a c_to_a A[x] A[y] A[z] T/|U|" \
var="#a = 0.666667" \
var="#Om = 0.318340; Ol = 0.681660; h = 0.670423" \
var="#FOF linking length: 0.280000" \
var="#Unbound Threshold: 0.500000; FOF Refinement Threshold: 0.700000" \
var="#Particle mass: 8.22651e+10 Msun/h" \
var="#Box size: 3999.930664 Mpc/h" \
var="#Force resolution assumed: 0.0462767 Mpc/h" \
var="#Units: Masses in Msun / h" \
var="#Units: Positions in Mpc / h (comoving)" \
var="#Units: Velocities in km / s (physical, peculiar)" \
var="#Units: Halo Distances, Lengths, and Radii in kpc / h (comoving)" \
var="#Units: Angular Momenta in (Msun/h) * (Mpc/h) * km/s (physical)" \
var="#Units: Spins are dimensionless" \
var="#Rockstar Version: 0.99.5 (Beta)"
 */

